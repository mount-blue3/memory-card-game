const cards = document.querySelectorAll(".card");
let cardOne, cardTwo;
let disableDeck = false;
let matchedCard = 0;
const time = document.querySelector(".timer");
const moves = document.querySelector(".moves");
const start = document.querySelector("button");
const state = {
  gameStarted: false,
  flippedCards: 0,
  totalFlips: 0,
  totalTime: 0,
  loop: null,
};
function startGame() {
  if (state.gameStarted) {
    resetGame();
    return;
  }
  state.gameStarted = true;
  start.classList.add("disabled");
  const startTime = Date.now();
  state.loop = setInterval(() => {
    const currentTime = Date.now();
    const elapsedTime = Math.floor((currentTime - startTime) / 1000);
    const minutes = Math.floor(elapsedTime / 60);
    const seconds = elapsedTime % 60;
    state.totalTime = elapsedTime;
    moves.innerText = `${state.totalFlips} moves`;
    time.innerText = `Time: ${minutes} min ${seconds} sec`;
  }, 1000);
}
function stopGame() {
  clearInterval(state.loop);
  start.classList.remove("disabled");
}
function resetGame() {
  //location.reload();
  matchedCard = 0;
  cardOne = null;
  cardTwo = null;
  disableDeck = false;
  state.flippedCards = 0;
  state.totalFlips = 0;
  state.totalTime = 0;
  clearInterval(state.loop);
  cards.forEach((card) => {
    card.classList.remove("flip");
    card.addEventListener("click", flipCard);
  });
  moves.innerText = "0 moves";
  time.innerText = "Time: 0 min 0 sec";
  start.classList.remove("disabled");
  state.gameStarted = false;
}
function flipCard(e) {
  if (!state.gameStarted) {
    return;
  }
  state.flippedCards++;
  state.totalFlips++;
  let clickedCard = e.target;

  if (clickedCard !== cardOne && !disableDeck) {
    clickedCard.classList.add("flip");

    if (!cardOne) {
      return (cardOne = clickedCard);
    }
    cardTwo = clickedCard;

    disableDeck = true;

    let cardOneImg = cardOne.querySelector("img").src,
      cardTwoImg = cardTwo.querySelector("img").src;
    matchCards(cardOneImg, cardTwoImg);
  }
}

function matchCards(img1, img2) {
  if (img1 === img2) {
    matchedCard++;
    cardOne.removeEventListener("click", flipCard);
    cardTwo.removeEventListener("click", flipCard);
    cardOne = cardTwo = "";
    if (matchedCard === cards.length / 2) {
      stopGame();
      setTimeout(() => {
        return shuffleCard();
      }, 1200);
      setTimeout(() => {
        showCongratulations();
        setTimeout(() => {
          resetGame();
        }, 3000);
      }, 2000);
    }
    return (disableDeck = false);
  } else {
    setTimeout(() => {
      cardOne.classList.remove("flip");
      cardTwo.classList.remove("flip");
      cardOne = cardTwo = "";

      disableDeck = false;
    }, 1000);
  }
}
function showCongratulations() {
  const ribbonContainer = document.querySelector(".ribbon-container");
  ribbonContainer.style.display = "block";
  setTimeout(hideCongratulations, 2000);
}
function hideCongratulations() {
  const ribbonContainer = document.querySelector(".ribbon-container");
  ribbonContainer.style.display = "none";
}
function shuffleCard() {
  const cardArray = Array.from(cards);
  cardArray.sort(() => Math.random() - 0.5);
  const deck = document.querySelector(".cards");
  deck.innerHTML = "";
  cardArray.forEach((card) => deck.appendChild(card));
}
start.addEventListener("click", startGame);
cards.forEach((card) => {
  card.addEventListener("click", flipCard);
});
hideCongratulations();
shuffleCard();
